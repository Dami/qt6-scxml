Document: qt6-scxml
Title: Debian qt6-scxml Manual
Author: <insert document author here>
Abstract: This manual describes what qt6-scxml is
 and how it can be used to
 manage online manuals on Debian systems.
Section: unknown

Format: debiandoc-sgml
Files: /usr/share/doc/qt6-scxml/qt6-scxml.sgml.gz

Format: postscript
Files: /usr/share/doc/qt6-scxml/qt6-scxml.ps.gz

Format: text
Files: /usr/share/doc/qt6-scxml/qt6-scxml.text.gz

Format: HTML
Index: /usr/share/doc/qt6-scxml/html/index.html
Files: /usr/share/doc/qt6-scxml/html/*.html
